import { Job } from 'bull'
import { log } from '../../logger'
import * as MailService from '../../../services/MailService'
import { getSessionsWithPipeline } from '../../../models/Session/queries'
import { getVolunteerContactInfoById } from '../../../models/Volunteer/queries'
import { USER_SESSION_METRICS, FEEDBACK_VERSIONS } from '../../../constants'
import { asObjectId } from '../../../utils/type-utils'

/**
 *
 * conditions for sending the email:
 * - partner volunteer who has had 10 sessions
 * - each session must be 15+ minutes long and must have a session flag for an absent user
 * - must have not left 3 ratings of 1 - 3 session ratings
 *
 */

interface EmailTenSessionJobData {
  volunteerId: string
  firstName: string
  email: string
}

export default async (job: Job<EmailTenSessionJobData>): Promise<void> => {
  const {
    data: { firstName, email },
    name: currentJob,
  } = job
  const volunteerId = asObjectId(job.data.volunteerId)
  const volunteer = await getVolunteerContactInfoById(volunteerId)
  // Do not send email if volunteer does not match email recipient spec
  if (!volunteer) return

  const fifteenMins = 1000 * 60 * 15
  // TODO: repo pattern
  const sessions = await getSessionsWithPipeline([
    {
      $match: {
        volunteer: volunteerId,
        timeTutored: { $gte: fifteenMins },
        reviewFlags: {
          $nin: [
            USER_SESSION_METRICS.absentStudent,
            USER_SESSION_METRICS.absentVolunteer,
          ],
        },
      },
    },
    {
      $lookup: {
        from: 'feedbacks',
        localField: 'volunteer',
        foreignField: 'volunteerId',
        as: 'feedbacks',
      },
    },
    {
      $lookup: {
        from: 'feedbacks',
        let: {
          volunteerId: '$volunteer',
          sessionId: '$_id',
        },
        pipeline: [
          {
            $match: {
              $expr: {
                $and: [
                  { $eq: ['$sessionId', '$$sessionId'] },
                  { $eq: ['$volunteerId', '$$volunteerId'] },
                ],
              },
            },
          },
        ],
        as: 'feedback',
      },
    },
    {
      $unwind: {
        path: '$feedback',
        preserveNullAndEmptyArrays: true,
      },
    },
    {
      $project: {
        _id: 1,
        sessionRating: {
          $switch: {
            branches: [
              {
                case: {
                  $and: [
                    {
                      $eq: ['$feedback.versionNumber', FEEDBACK_VERSIONS.ONE],
                    },
                    '$feedback.responseData.session-rating.rating',
                  ],
                },
                then: '$feedback.responseData.session-rating.rating',
              },
              {
                case: {
                  $and: [
                    {
                      $eq: ['$feedback.versionNumber', FEEDBACK_VERSIONS.TWO],
                    },
                    '$feedback.volunteerFeedback.session-enjoyable',
                  ],
                },
                then: '$feedback.volunteerFeedback.session-enjoyable',
              },
            ],
            default: null,
          },
        },
      },
    },
  ])

  if (sessions.length === 10) {
    let totalLowSessionRatings = 0
    const lowSessionRating = 3
    const totalLowSessionRatingsLimit = 3
    for (const session of sessions) {
      if (
        typeof session.sessionRating === 'number' &&
        session.sessionRating <= lowSessionRating
      )
        totalLowSessionRatings++
    }
    if (totalLowSessionRatings >= totalLowSessionRatingsLimit) return

    try {
      await MailService.sendPartnerVolunteerTenSessionMilestone(
        email,
        firstName
      )
      log(`Sent ${currentJob} to volunteer ${volunteerId}`)
    } catch (error) {
      throw new Error(
        `Failed to send ${currentJob} to volunteer ${volunteerId}: ${error}`
      )
    }
  }
}
