import 'newrelic'
import { connect } from './db'
import initializeUnleash from './utils/initialize-unleash'
import rawConfig from './config'
import { Config } from './config-type'
import app from './app'
import logger from './logger'
import { registerListeners } from './services/listeners'

async function main() {
  try {
    Config.check(rawConfig)
  } catch (err) {
    throw new Error(`error parsing config on startup: ${err}`)
  }

  initializeUnleash()

  try {
    await connect()
  } catch (err) {
    throw new Error(
      `db connection failed after backoff attempts, exiting: ${err}`
    )
  }

  registerListeners()

  const port = process.env.PORT || 3000
  app.listen(port, () => {
    logger.info('api server listening on port ' + port)
  })
}

try {
  main()
} catch (err) {
  logger.error(err as Error)
  process.exit(1)
}
